import React from 'react';
import './style.css';
import businnes from '../../images/businnes.svg';
import reliability from '../../images/confiabilidade.svg';
import faster from '../../images/expanda.svg';
import owner from '../../images/organize_owner.svg';
import toUp from '../../images/toup.svg';


const Main = () => ( 
    <main>
        <div className="Main">
            <ul className='ulEl'>
                <li><img src={businnes}/></li>
                <li><img src={reliability}/></li>
                <li><img src={toUp}/></li>
                <li><img src={faster}/></li>
                <li><img src={owner}/></li>
                
            </ul>
        </div>
    </main>
);


export default Main;



