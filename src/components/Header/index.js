import React from 'react';
import './style.css';
import logo from "../../images/1.png";
import barber from "../../images/barber.png";

//Isso se iguala a função principal do react

/*
    clas Header extends Component {
        render(){
            ...
        }
    }
*/

const Header = () => (
    <header id='header-el'>
        <div className='info-header'>
            <div className='logo'>
                <img src={logo}></img>
            </div>
            <ul className='listEl'>
                <li><a href="#">Home</a></li>
                <li><a href="#">Contact</a></li>
                <li><a href="#">to Know</a></li>
            </ul>
        </div>
        <div className="header-descr">
            <h1>Barber Origins</h1>
            <p>Cabeleleiro(a),Barbeiro(a) venha transformar seu empreendimento !</p>
        </div>
    </header>
);


export default Header;

